A thread to position myself
---

In this repository, created at the [GLAMhack 2023](https://opendata.ch/events/glamhack2023/) event, we are creating a digital interactive exhibition installation on slavery for the online platform of the exhibition 'Memories' at the [Geneva Ethnography Museum](https://en.wikipedia.org/wiki/Mus%C3%A9e_d%27ethnographie_de_Gen%C3%A8ve) (MEG).

# Challenge description

_As part of the MEG's upcoming temporary exhibition, 'Memories. Geneva in the Colonial World', the interpretation team and Ivonne Gonzales, has designed a physical participative installation on the topic of slavery. The installation’s aim is to encourage the visitors to move beyond a solely intellectual (and thus distant) understanding of the transatlantic trade and its devasting consequences. Instead, the installation will ask the public to position themselves emotionally and intellectually vis-à-vis this tragic chapter of our common history._

# Results

- [Web site prototype](https://colonialgeneva.ch/threads2/)
- [GLAMhack project page](https://hack.glam.opendata.ch/project/201)
- [Sketch made with 🏴‍☠️ P5.js](https://openprocessing.org/sketch/2026003) (`sketch.js` in this repo) - [Preview](https://glamhack23.colonialgeneva.ch/threads/)
- [Sketch made with 🤖 GPT](https://openprocessing.org/sketch/2026567) (`GPT_p5.js`) - [Preview](https://glamhack23.colonialgeneva.ch/threads/index-gpt.html)
- [Penpot user interface mockup](https://design.penpot.app/#/view/1c096c09-a10e-80c0-8003-33a9f0727eb9?page-id=1c096c09-a10e-80c0-8003-33a9f0727eba&section=interactions&index=0&share-id=1c096c09-a10e-80c0-8003-349ca531e470) (`mockup` folder)
- [Online form for data collection](https://glamhack23.colonialgeneva.ch/formtools/modules/form_builder/published/threadsform.php)

# Resources

We built this project using the open source [P5.js](https://p5js.org/) library, using [OpenProcessing](https://openprocessing.org) and [OpenAI](https://chat.openai.com/) to accelerate the process.

Our inspirations included:

- [Connecting-Dots](https://github.com/ThugRaven/Connecting_Dots), a P5.js project by Kamil Wesołowski - some of whose code we reused.
- [Force-directed D3 graph](https://observablehq.com/@d3/disjoint-force-directed-graph/2?intent=fork) (thanks Valerio) - for the Web-native orientation.
- [To Me America Is](https://bucketeer-036aa605-c047-4623-8610-f1764b90cf98.s3.amazonaws.com/glamhack/11558/4L01LZC4G41VN0UK8B3I354P/Image2.jpg) on Instagram by @Lille - for the tangible installation.

A basic sketch of the idea was provided in the challenge:

![](https://bucketeer-036aa605-c047-4623-8610-f1764b90cf98.s3.amazonaws.com/glamhack/11558/P1XJR44MU4LEV7X18VG4UX74/dispositif_mediation.png)

From this, we made Processing sketches, the "Potato prototypes" as we casually referred to them - on one hand, by reusing [open source code](https://github.com/ThugRaven/Connecting_Dots/tree/master), and on the other hand, with prompts to LLMs ([ChatGPT](https://chat.openai.com/share/107eb3ee-4531-4ccb-87bd-f6d98c70dbc6) and [Llama](https://huggingface.co/chat/)):

<img src="https://bucketeer-036aa605-c047-4623-8610-f1764b90cf98.s3.amazonaws.com/glamhack/1/35EXIFQEX2VCDECTWS2NX7W9/Screenshot_from_20230929_175347.png" width="42%"><img src="https://bucketeer-036aa605-c047-4623-8610-f1764b90cf98.s3.amazonaws.com/glamhack/11575/16M5FR7DSJPCNVIDJ1AQFZYA/Screenshot_20230930_at_12.31.08.png" width="42%">

Once we had the user interface working more or less how we liked, we created a [Gitea](https://gitea.com/) project repository, uploaded the codes, and started a [Kanban](https://de.wikipedia.org/wiki/Kanban-Board)-style Project to prioritize the work in the remaining hours of the hackathon. Thanks to [Wikipedia](https://en.wikipedia.org/wiki/Ellipse), we found the right formulas for our calculations.

<img title="Gitea" src="https://bucketeer-036aa605-c047-4623-8610-f1764b90cf98.s3.amazonaws.com/glamhack/1/MU48HZ28LAQEBYPK2GT36I68/Screenshot_from_20230930_203110.png" width="42%"><img title="Wikipedia" src="https://bucketeer-036aa605-c047-4623-8610-f1764b90cf98.s3.amazonaws.com/glamhack/1/30AMY5FWS9ED721GJXXTRRL5/Screenshot_from_20230930_005108.png" width="42%">

Using the open source [Penpot app](https://Penpot.app) our team could quickly design the user journey through a series of pop-ups. These give a good idea of how we plan to collect data. Once exported in SVG format (Code property in the inspector to the right), we can connect them to our real application using JavaScript code.

![Screenshot of Penpot app](https://bucketeer-036aa605-c047-4623-8610-f1764b90cf98.s3.amazonaws.com/glamhack/1/TS6P9IEKA4IM5NUJM1WGIODO/Screenshot_20231001085631.png) 

In the meantime, we used the [Dribdat](https://dribdat.cc) platform of the hackathon to share links and collaborate. We also got access to a sandbox server where we could deploy our solution: we installed [Wordpress](https://wordpress.org) with the [Brizy](https://brizy.io/) plugin for the main exhibition website, [Form Tools](https://formtools.org/) for designing the data collection process, [TreeQL](https://www.treeql.org/) and [phpMyAdmin](https://www.phpmyadmin.net/) to manage the resulting database.

<img title="Dribdat" src="https://bucketeer-036aa605-c047-4623-8610-f1764b90cf98.s3.amazonaws.com/glamhack/1/POWCUEH05Y5KJGFMVEP1PI8M/Screenshot_from_20230930_203926.jpg" width="42%"><img title="Form Tools" src="https://bucketeer-036aa605-c047-4623-8610-f1764b90cf98.s3.amazonaws.com/glamhack/1/J6ZCN6C57R1CNW1E3QWP8TW1/Screenshot_from_20230930_203750.png" width="42%">

We ran out of time to get a proper API developed, but managed to submit the `FORM POST` data from our visualization to Form Tools. This proof of concept was demonstrated at the end of the GLAMhack event, along with our notes, expressing a strong interest from the group to further support the innovative development of exhibitions at the MEG.

<img src="https://bucketeer-036aa605-c047-4623-8610-f1764b90cf98.s3.amazonaws.com/glamhack/1/ALC8YZOH4GOGDDM7K88NL7ZE/IMG_20230930_15031201.jpeg" width="42%" title="The Team">

Further work would include refining the design, completing functionality of the interactive application, working on Accessibility and usability of the solution, better supporting touchscreen devices, examining Data Protection and Security of the solution, and further testing and refining the result in an agile way. 

Thanks to the OpenGLAM community and the other Hackathon projects, we also have ideas on how to add scientific depth to the exhibition and installation using explorative data techniques. [Follow our Issues](https://codeberg.org/OpenGLAM-CH/threads/issues) or contact us to stay tuned.

# Team

[![](https://www.gravatar.com/avatar/e25e5438563a64ad9900a9b53d27a962?d=retro&s=80) Emilie](https://hack.glam.opendata.ch/user/EmilieThvenoz)
[![](https://www.gravatar.com/avatar/d5f572d38d0feeb3b8dde053fbc09f8c?d=retro&s=80) Giuachin](https://hack.glam.opendata.ch/user/Giuachin)
[![](https://www.gravatar.com/avatar/82027a30c56aa35fd193034cc9db0acc?d=retro&s=80) Erica](https://hack.glam.opendata.ch/user/erica_brztg)
[![](https://www.gravatar.com/avatar/137236c8236e540c76b93cdf81a7a55f?d=retro&s=80) Iris](https://hack.glam.opendata.ch/user/Iris_Terradura)
[![](https://www.gravatar.com/avatar/fdb99ee5ea43425c1e42833cca543887?d=retro&s=80) Oleg](https://hack.glam.opendata.ch/user/loleg)

# Merci

🦸 [Grégoire](https://www.geneve.ch/fr/autorites-administration/administration-municipale/annuaire-ville-geneve/ceuninck-gregoire) et 🧑‍🎨 [Valerio](https://phabricator.wikimedia.org/p/valerio.bozzolan/)

# License

This work is licensed under [CC BY 4.0 International](https://creativecommons.org/licenses/by/4.0/)